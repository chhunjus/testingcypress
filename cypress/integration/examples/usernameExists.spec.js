/// <reference types="cypress" />

describe("UserName Exists", ()=>{

  it("username exists via UI",()=>{
    cy.visit('http://localhost:4100/register')
    cy.get('input[type="text"]').type('sharadllo')
    cy.get('input[type="email"]').type('fasdfasdfo@jake.jake')
    cy.get('input[type="password"]').type('hefdsfdsfllo')
    cy.get('button[type="submit"]').click()
    cy.get('.error-messages > :nth-child(1)').should('contain.text','username has already been taken')
  })

  it("username exists from real API response without UI",()=>{
    cy.request({
      method:'POST',
      url: `https://conduit.productionready.io/api/users`,
      failOnStatusCode: false,
      body: {
          "user":{
            "username": "Jacob",
            "email": "jake@jake.jake",
            "password": "jakejake"
            }
        }
      }).then((resp)=>{
          expect(resp.body.errors.username[0]).to.contain('has already been taken')
        })
  })

  it("send our own response (error displays or not in UI), whether api response is displayed or not",()=>{
    cy.visit('http://localhost:4100/register')
    cy.server()
    cy.route({
      method:'POST',
      status:422,
      url: `**/api/users`,
      response: {
        "errors": {   
          "username":["pahila nai lisakyo"],
          "email":["hello fsadhfkjadsh"]
        }
      }
    }).as('q')
    cy.get('button[type="submit"]').click()
      cy.wait('@q')
      cy.get('.error-messages > :nth-child(1)').should('contain.text','username pahila nai lisakyo')
  })
})