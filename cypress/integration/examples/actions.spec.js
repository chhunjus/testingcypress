/// <reference types="cypress" />

describe("Set Jwt in local storage", ()=>{

  it("Goes to settings",()=>{
    cy.request({
      method: 'POST',
      url: 'https://conduit.productionready.io/api/users/login',
      body:{
        "user":{
          "email": "hello@jake.jake",
          "password": "jakejake"
        }
      }
    }).then((resp) =>{
      window.localStorage.setItem('jwt', resp.body.user.token)
      cy.visit(`http://localhost:4100/settings`)
    })

  })
})