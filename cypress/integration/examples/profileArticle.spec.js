/// <reference types="cypress" />

describe("Article Check", ()=>{

    beforeEach('it logins',()=>{
        cy.server()
        cy.route('GET', `**/articles?author*`,'fixture:favArticle.json').as('w')
        cy.login()
                
    })

    it('checks my article',()=>{
        cy.route('GET', `**/articles?favorited*`,'fixture:fArticle.json').as('q')
        cy.visit('http://localhost:4100/@sharadllo')
        cy.wait('@w')

        cy.contains('Favorited Articles').click()
        cy.wait('@q')
        cy.fixture('fArticle').then(file =>{
            const slugs = file.articles[0].slug
            cy.route
            cy.route('GET', '**/articles/'+slugs+'/comments','fixture:comments.json')
            cy.route('GET', '**/articles/'+slugs, 'fixture:fArticle.json')
        })
        cy.get('.preview-link').click()
    })
  })