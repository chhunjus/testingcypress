/// <reference types="cypress" />

describe('Stubbing for articles',()=>{

    beforeEach('it logins',()=>{
        cy.server()
        cy.route('GET', '**/tags', 'fixture:tags.json').as('tags')

        //intercept not working
        // cy.intercept('GET','**/tags', {fixture:'tags.json'})
        cy.login()
        cy.visit('/')
    })

    it('verify tags',()=>{
        cy.wait('@tags')
        cy.get('.tag-list')
        .should('contain','car')
        .should('contain','bike')
        .and('contain','ship')
    })

    it('article',()=>{
    //     cy.visit('/login')
    //     cy.get('input[type="email"]').type('hello@jake.jake')
    //   cy.get('input[type="password"]').type('jakejake')
    //   cy.get('button[type="submit"]').click()
      cy.server()
        cy.route('GET','**/articles/feed*','{"articles":[],"articlesCount":0}')
        cy.route('GET','**/articles*','fixture:multipleArticles.json')
        cy.contains('Global Feed').click()


        cy.get('.article-preview button').then(listOfButtons =>{
            expect(listOfButtons[2]).to.contain('50')
            expect(listOfButtons[0]).to.contain('9')
        })


        //getting problem in slug
        cy.fixture('multipleArticles').then(file =>{
            const slugs = file.articles[2].slug
            cy.route('POST', '**/articles/'+slugs+'/favorite', file)
        })
        cy.get('.article-preview button')
        .eq(1).click()
        .contains('1')
    
    })
})